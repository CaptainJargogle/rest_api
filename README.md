This is the server side to a bigger project.
It's a REST Api using flask that communicats with an sqlalchemy database.
When a request with a word/sentence attached to it the response of the server comes back 
as a list of positions that's going to be used by our hardware.

This project is a composition between flask and nginx hence the docker-compose file.

To Run:
-run: docker-compose up
(to get the server running) 
-run: python3 testRequest.py 
(to write the word/sentence to be translated and get the server's response)


