from flask import Flask, request, jsonify, render_template
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow  #an object serialization/deserialization library
import os




# Init app
app= Flask(__name__)

#Init ma
ma= Marshmallow(app)

#Correctly locating the databse file
basedir = os.path.abspath(os.path.dirname(__file__))

#Database
app.config["SQLALCHEMY_DATABASE_URI"]= "sqlite:///" + os.path.join(basedir, "signs.db")

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] =False

#init db
db= SQLAlchemy(app)

#Sign class/model

class Sign(db.Model):
    __tablename__="Sign"
    idS= db.Column("idS", db.Integer, primary_key=True)
    char= db.Column("char", db.String(1))
    thumb1= db.Column("thumb1", db.Float)
    thumb2= db.Column("thumb2", db.Float)
    index= db.Column("index", db.Float)
    middle= db.Column("middle", db.Float)
    ring= db.Column("ring", db.Float)
    pinky= db.Column("pinky", db.Float)



# Sign Schema
class SignSchema (ma.Schema):
    class Meta:
        fields = ("idS","char", "thumb1","thumb2", "index", "middle", "ring","pinky")

#Init Schema
sign_schema = SignSchema()
signs_schema = SignSchema(many=True, )


#Get all Signs
@app.route("/signs", methods=["GET"])
def get_signs():
    all_signs= Sign.query.all()
    result = signs_schema.dump(all_signs)
    return jsonify(result)


#Get single Sign 
@app.route("/signs/<text>", methods=["GET"])
def get_sign(text):
    letters=[]
   
    for c in text:
        if c == "-":
            letters.append([0.0,0.0,0.0,0.0,0.0,0.0])
        else:
            tmp=Sign.query.filter_by(char=c).first()
            t=[tmp.thumb1,tmp.thumb2,tmp.index,tmp.middle,tmp.ring,tmp.pinky]  
            letters.append(t) 
        
    return jsonify(letters)






#default
@app.route("/")
def hello():
    return render_template('home.html')

  




#Run Server
if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0',port=5000)


